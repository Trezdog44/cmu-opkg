# THIS REPOSITORY UNDER DEVELOPMENT.

![architecture](images/architecture.jpg)

# CMU Package Manager
> The [opkg](https://wiki.openwrt.org/doc/techref/opkg) utility (an ipkg fork) is a lightweight package manager used to download and install OpenWrt packages from local package repositories or ones located in the Internet. /cc [wiki.openwrt.org](https://wiki.openwrt.org/doc/techref/opkg)

# For User
- Use USB installer USB script. https://gitlab.com/mzdonline/cmu-opkg/tree/master/installer 
- or follow [this](https://mzdonline.wordpress.com/2017/07/23/ติดตั้ง-opkg-ใน-mzd-connect/) instruction.

```bash
opkg update
opkg install headunit
opkg upgrade
```

# Demo
[![asciicast](https://asciinema.org/a/9zbtx265iul13k1ttpjt0067n.png)](https://asciinema.org/a/9zbtx265iul13k1ttpjt0067n)

[![asciicast](https://asciinema.org/a/gBnH2vuwAihgDWQxLt5RVdL0B.png)](https://asciinema.org/a/gBnH2vuwAihgDWQxLt5RVdL0B)

# For Developer
Developer can create package like Openwrt.  I use `opkg-build` tools to create packages.

## Basic command
```
git clone git://git.yoctoproject.org/opkg-utils
cd opkg-utils
sudo make install

git clone https://khantaena@gitlab.com/mzdonline/cmu-opkg.git
cd cmu-opkg/example
opkg-build headunit
opkg-make-index . > Packages
gzip -k Packages
```
Upload {headuniit-x-x.ipk,Packages,Packages.gz} to server. and edit `opkg.conf` point to server URL. 

## Compile sources

ouput is headunit_x_x.ipk

```bash
git clone https://gitlab.com/mzdonline/cmu-opkg.git
cd cmu-opkg/ports/
. environment
cd headunit
make
```
