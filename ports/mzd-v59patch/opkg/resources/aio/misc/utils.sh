#!/bin/sh
# tweaks.sh - MZD-AIO-TI Version 2.7.0
# Special thanks to Siutsch for collecting all the tweaks and for the original AIO
# Big Thanks to Modfreakz, khantaena, Xep, ID7, Doog, oz_paulb.
# For more information visit http://mazdatweaks.com
# Enjoy, Trezdog44 - Trevelopment.com
# Change : Khantaena
#  - 24Oct2017: utils.sh

# AIO Variables
AIO_VER=2.7.0
AIO_DATE=2017.09.22

KEEPBKUPS=0

timestamp()
{
  date +"%D %T"
}
get_cmu_sw_version()
{
  _ver=$(grep "^JCI_SW_VER=" /jci/version.ini | sed 's/^.*_\([^_]*\)\"$/\1/')
  _patch=$(grep "^JCI_SW_VER_PATCH=" /jci/version.ini | sed 's/^.*\"\([^\"]*\)\"$/\1/')
  _flavor=$(grep "^JCI_SW_FLAVOR=" /jci/version.ini | sed 's/^.*_\([^_]*\)\"$/\1/')

  if [ ! -z "${_flavor}" ]; then
    echo "${_ver}${_patch}-${_flavor}"
  else
    echo "${_ver}${_patch}"
  fi
}

get_cmu_ver()
{
  _ver=$(grep "^JCI_SW_VER=" /jci/version.ini | sed 's/^.*_\([^_]*\)\"$/\1/' | cut -d '.' -f 1)
  echo ${_ver}
}

compatibility_check()
{
  # Compatibility check falls into 5 groups:
  # 59.00.5XX ($COMPAT_GROUP=5) When it is cracked 500+ will be compatible
  # 59.00.4XX ($COMPAT_GROUP=4)
  # 59.00.3XX ($COMPAT_GROUP=3)
  # 58.00.XXX ($COMPAT_GROUP=2)
  # 55.00.XXX - 56.00.XXX ($COMPAT_GROUP=1)
  _VER=$(get_cmu_ver)
  _VER_EXT=$(grep "^JCI_SW_VER=" /jci/version.ini | sed 's/^.*_\([^_]*\)\"$/\1/' | cut -d '.' -f 3)
  _VER_MID=$(grep "^JCI_SW_VER=" /jci/version.ini | sed 's/^.*_\([^_]*\)\"$/\1/' | cut -d '.' -f 2)
  if [ $_VER_MID -ne "00" ] # Only development versions have numbers other than '00' in the middle
  then
    echo 0 && return
  fi
  if [ $_VER -eq 55 ] || [ $_VER -eq 56 ]
  then
    echo 1 && return
  elif [ $_VER -eq 58 ]
  then
    echo 2 && return
  elif [ $_VER -eq 59 ]
  then
    if [ $_VER_EXT -lt 400 ] # v59.00.300-400
    then
      echo 3 && return
    elif [ $_VER_EXT -lt 500 ] # v59.00.400-500
    then
      echo 4 && return
    elif [ $_VER_EXT -eq 502 ]
    then
      echo 5 && return # 59.00.502 is another level because it is not compatible with USB Audio Mod
    else
      echo 0 && return
    fi
  else
    echo 0
  fi
}

log_message()
{
#  echo "$*" 1>&2
#  echo "$*" >> "/tmp/root/AIO_log.txt"
  /bin/fsync "/tmp/root/AIO_log.txt"
}

aio_info()
{
  if [ ${KEEPBKUPS} -eq 1 ]
  then
    echo "$*" 1>&2
    echo "$*" >> "${MYDIR}/AIO_info.json"
    /bin/fsync "${MYDIR}/AIO_info.json"
  fi
}

remove_aio_css()
{
  sed -i "/.. MZD-AIO-TI *${2} *CSS ../,/.. END AIO *${2} *CSS ../d" "${1}"
  INPUT=${1##*/}
  log_message "===                Removed ${2} CSS From ${INPUT}                ==="
}

remove_aio_js()
{
  sed -i "/.. MZD-AIO-TI.${2}.JS ../,/.. END AIO.${2}.JS ../d" "${1}"
  INPUT=${1##*/}
  log_message "===              Removed ${2} JavaScript From ${INPUT}             ==="
}

show_message()
{
  sleep 5
  killall jci-dialog
  #	log_message "= POPUP: $* "
  /jci/tools/jci-dialog --info --title="MZD-AIO-TI  v.${AIO_VER}" --text="$*" --no-cancel &
}

show_message_OK()
{
  sleep 4
  killall jci-dialog
  #	log_message "= POPUP: $* "
  /jci/tools/jci-dialog --confirm --title="MZD-AIO-TI | CONTINUE INSTALLATION?" --text="$*" --ok-label="YES - GO ON" --cancel-label="NO - ABORT"
  if [ $? != 1 ]
  then
    killall jci-dialog
    return
  else
    log_message "********************* INSTALLATION ABORTED *********************"
    sleep 10
    killall jci-dialog
    exit 0
  fi
}

